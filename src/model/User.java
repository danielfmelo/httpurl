package model;

public class User {

    String gender;
    String email;
    String phone;
    Name name;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return " =========== User ============" + '\n' +
                "name: " + name + '\n' +
                "email: " + email + '\n' +
                "phone: " + phone + '\n' +
                "gender: " + gender + '\n';
    }
}
