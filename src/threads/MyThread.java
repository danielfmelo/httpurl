package threads;

import http.HTTPExample;
import model.User;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class MyThread implements Runnable {
    private String url;
    private String name;

    public MyThread(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public static String objectToJSON(User student) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(student);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return s;
    }

    public static List<User> JSONToObject(String s) {

        ObjectMapper mapper = new ObjectMapper();
        List<User> users = null;

        try {
            Map<String,Object> map = mapper.readValue(s, Map.class);
            users =
                    mapper.readValue(new ObjectMapper().writeValueAsString(map.get("results")),
                            new TypeReference<List<User>>(){});
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }



        return users;
    }

    public static String getUsers(String string) {

        String content="";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setRequestProperty("Content-Type", "application/json");

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;
    }

    public static Map getHttpHeaders(String string) {
        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();

        } catch(Exception e) {
            System.err.println(e.toString());
        }

        return hashmap;

    }

    public void run() {
        System.out.println("Initiating Request: " + name);

        String json = this.getUsers(this.url);

        System.out.println(json);

        System.out.println("\n\n request " + name + " is done.\n\n");
    }
}
