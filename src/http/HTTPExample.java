package http;

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import model.User;

import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import threads.MyThread;

public class HTTPExample {

    public static void main(String[] args) {
        //URL used for the request

        String uri1 = "https://randomuser.me/api/?results=50&inc=gender,email,phone,name";
        String uri2 = "https://randomuser.me/api/?results=20&inc=gender,email,phone,name";
        String uri3 = "https://randomuser.me/api/?results=10&inc=gender,email,phone,name";
        String uri4 = "https://randomuser.me/api/?results=5&inc=gender,email,phone,name";

        ExecutorService myService = Executors.newFixedThreadPool(3);

        MyThread ds1 = new MyThread(uri1, "1");
        MyThread ds2 = new MyThread(uri2, "2");
        MyThread ds3 = new MyThread(uri3, "3");
        MyThread ds4 = new MyThread(uri4, "4");

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);

        myService.shutdown();
    }
}
